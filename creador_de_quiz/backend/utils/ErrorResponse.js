module.exports = {
    get_questionnaire_failed_response: {
        type: 'Questionare',
        id: 1,
        name: 'Ups! we had a problem retriving your questionnaire',
        description: 'You must reload the page to keep going',
        questions: [
            {
                type: 'Question',
                id: 1,
                text: 'Would you please reload the page?',
                answers: [
                    {
                        type: 'Answer',
                        id: 1,
                        idQuestion: 1,
                        text: 'Of course!',
                        correct: true,
                    },
                    {
                        type: 'Answer',
                        id: 2,
                        idQuestion: 1,
                        text: 'Hell yeah!',
                        correct: true,
                    },
                    {
                        type: 'Answer',
                        id: 3,
                        idQuestion: 1,
                        text: 'You are so bad guys but I will!',
                        correct: true,
                    },
                ],
            }
        ],
    },
    add_question_failed_response : {
        type: 'Question',
        id: 1,
        text: 'We had problems adding your question, Would you please reload the page and try again?',
        answers: [
            {
                type: 'Answer',
                id: 1,
                idQuestion: 1,
                text: 'Of course!',
                correct: true,
            },
            {
                type: 'Answer',
                id: 2,
                idQuestion: 1,
                text: 'Hell yeah!',
                correct: true,
            },
            {
                type: 'Answer',
                id: 3,
                idQuestion: 1,
                text: 'You are so bad guys but I will!',
                correct: true,
            },
        ],
    }
}
module.exports.SCHEMA = `
type Query {
  getQuestionnaire(id: Int!): Questionnaire!,
}  

type Mutation { 
  addQuestion(question: QuestionInput!, questionnaireId: Int!): Question!    
}

type Answer { 
  idQuestion: Int,
  type: String,
  id: Int!, 
  text: String!, 
  correct: Boolean!
}

type Question { 
  type: String!,
  id: Int!,
  text: String!,
  answers:[Answer!]!
}

type Questionnaire { 
  id: Int!,
  name: String!,
  description: String!,
  questions: [Question]!
}

input QuestionInput {     
  text: String!,
  answers:[AnswerInput!]!
}

input AnswerInput {         
  text: String!, 
  correct: Boolean!
}
`
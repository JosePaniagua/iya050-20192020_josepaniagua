const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();


const { SCHEMA } = require('./schema');
const { RESOLVERS } = require('./resolvers');
const PORT = process.env.PORT || 4000;

const schema = buildSchema(SCHEMA);

const app = express();

//CORS POLICY
app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: RESOLVERS,
    graphiql: true,
}));

app.listen(PORT, () => console.log(`App running on port ${PORT}`));
const {
    get_questionnaire_failed_response,
    add_question_failed_response
} = require('./utils/ErrorResponse');

const QUESTION_TYPE = 'Question';
const ANSWER_TYPE = 'Answer';

const fetch = require('node-fetch');
const APPLICATION_KEY = '70726f6772616d616';

module.exports.RESOLVERS = {
    getQuestionnaire: ({ id }) => getQuestionnaire(id)
        .then(value => value.find(questionnaire => questionnaire.id === id))
        .catch(error => {
            console.log('there was an error while retriving the questionnaires ', error);
            return get_questionnaire_failed_response;
        }),
    addQuestion: async (params) => {
        const { questionnaireId, question } = params
        try {
            const data_store = await getQuestionnaire(questionnaireId);
            const questionnaire = data_store.find(questionnaire => questionnaire.id === questionnaireId);
            question.type = QUESTION_TYPE; question.id = questionnaire.questions.length;
            question.answers = question.answers.map((answer, index) => ({
                ...answer,
                id: (index + 1),
                type: ANSWER_TYPE
            }));
            questionnaire.questions.push(question);
            await putQuestionnaires(data_store);
            return question;
        } catch (exception) {
            console.log(`there was a problem setting a new question for questionnaire ${questionnaireId} and 
            question ${JSON.stringify(question)}`);
            return add_question_failed_response;
        }

    }
};

const getQuestionnaire = () => fetch(`https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/${APPLICATION_KEY}`, {
    method: 'GET',
    headers: { 'x-application-id': 'jose.paniagua' },
})
    .then(res => res.json())
    .then(json_response => JSON.parse(json_response.value))

const putQuestionnaires = data => fetch(`https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/${APPLICATION_KEY}`, {
    method: 'PUT',
    headers: { 'x-application-id': 'jose.paniagua' },
    body: JSON.stringify(data)
})
    .then(res => res.json());    
const { React } = window;
const { useState, useContext } = React;
const { useHistory } = window.ReactRouterDOM;
const { QuestionaireContext, TextInput, AnswerInput } = window;

const QuestionForm = () => {
  const [text, setText] = useState('');
  const [answers, setAnswers] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const { id, name } = useContext(QuestionaireContext);
  const history = useHistory();

  function handleChangeText(e) {
    const { value } = e.target;

    setText(value);
  }

  function validateForm() {
    if (text === '') {
      alert('Question field is required');
      return false;
    }

    if (answers.length === 0) {
      alert('Answers field is required');
      return false;
    }

    const correctAnswers = answers.filter((a) => a.correct);

    if (correctAnswers.length === 0) {
      alert('At least one answer must be correct');
      return false;
    }

    return true;
  }

  function handleSubmit(e) {
    e.preventDefault();
    const isValid = validateForm();

    if (!isValid) return;

    setIsSubmitting(true);

    //QUERY CONFIG
    const { POST_QUESTION_QUERY } = window.queries;
    POST_QUESTION_QUERY.variables.idQuestionnaire = id;
    POST_QUESTION_QUERY.variables.question = { text, answers };
    console.log(JSON.stringify(POST_QUESTION_QUERY));
    fetch('http://ec2-34-247-162-212.eu-west-1.compute.amazonaws.com:4000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(POST_QUESTION_QUERY)
    }).then(response => response.json())
      .then(json_response => json_response.data)
      .then(data => {
        console.log("received data:  ", data);
        setIsSubmitting(false);
        history.push(`/questionaire/${id}`);
      })
      .catch(err => console.log('the error was: ', err));

    history.push();
  }

  return (
    <React.Fragment>
      <header className="header">
        <h1 className="header__title">Add Question to {name}</h1>
      </header>

      <form className="container widget-form" onSubmit={handleSubmit}>
        <TextInput
          style={{ marginBottom: 'var(--m-size)' }}
          placeholder="Question..."
          value={text}
          onChange={handleChangeText}
        />
        <AnswerInput answers={answers} onChange={setAnswers} />
        <button className="big-button" disabled={isSubmitting} type="submit">
          Submit
        </button>
      </form>
    </React.Fragment>
  );
};

window.QuestionForm = QuestionForm;

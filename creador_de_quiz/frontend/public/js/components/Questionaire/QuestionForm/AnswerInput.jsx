const { React } = window;
const { useEffect } = React;
const { TextInput, SelectInput } = window;

const AnswerInput = ({ answers, onChange }) => {
  function handleAdd() {
    const newAnswer = {
      text: '',
      correct: false,
    };

    onChange([...answers, newAnswer]);
  }

  function handleRemove(row) {
    const filteredAnswers = answers.filter((_, answerRow) => answerRow !== row);

    onChange(filteredAnswers);
  }

  function handleChange(newAnswer, row) {
    const newAnswers = answers.map((answer, answerRow) => {
      if (answerRow === row) {
        return newAnswer;
      }

      return answer;
    });

    onChange(newAnswers);
  }

  useEffect(() => {
    if (answers.length > 0) return;

    handleAdd();
  }, [answers]);

  if (answers.length === 0) return null;

  return (
    <table className="widget">
      <thead>
        <tr className="widget-header">
          <th className="widget__message">Answer</th>
          <th className="widget__message">Correct</th>
          <th className="widget__message">Remove</th>
        </tr>
      </thead>

      <tbody>
        {answers.map((answer, row) => (
          <tr key={row}>
            <td className="widget__message">
              <TextInput
                placeholder="Answer..."
                value={answer.text}
                onChange={(e) => {
                  const newAnswer = {
                    ...answer,
                    text: e.target.value,
                  };

                  handleChange(newAnswer, row);
                }}
              />
            </td>

            <td className="widget__message">
              <SelectInput
                value={Number(answer.correct)}
                onChange={(e) => {
                  const newAnswer = {
                    ...answer,
                    correct: Boolean(e.target.value),
                  };

                  handleChange(newAnswer, row);
                }}
              >
                <option value={1}>Yes</option>
                <option value={0}>No</option>
              </SelectInput>
            </td>

            <td className="widget__message">
              <button
                className="button button--link"
                onClick={(e) => {
                  e.preventDefault();
                  handleRemove(row);
                }}
              >
                Remove
              </button>
            </td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr className="widget-header">
          <td colSpan="3">
            <button
              className="button"
              style={{ width: '100%' }}
              onClick={(e) => {
                e.preventDefault();
                handleAdd();
              }}
            >
              Add Answer
            </button>
          </td>
        </tr>
      </tfoot>
    </table>
  );
};

window.AnswerInput = AnswerInput;

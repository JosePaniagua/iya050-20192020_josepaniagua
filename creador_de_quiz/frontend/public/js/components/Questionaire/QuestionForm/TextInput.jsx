const { React } = window;

const TextInput = ({ value, onChange, placeholder = '', ...props }) => (
  <input
    className="widget-form__input"
    type="text"
    placeholder={placeholder}
    value={value}
    onChange={onChange}
    {...props}
  />
);

window.TextInput = TextInput;

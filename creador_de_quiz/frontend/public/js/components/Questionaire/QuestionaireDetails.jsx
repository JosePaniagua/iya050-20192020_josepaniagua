const { React } = window;
const { Fragment, useContext } = React;
const { useHistory } = window.ReactRouterDOM;
const { QuestionaireContext } = window;

const QuestionaireDetails = ({ match }) => {
  const history = useHistory();
  const { name, description, questions } = useContext(QuestionaireContext);

  const idFirstQuestion = questions.length > 0 ? questions[0].id : null;

  function handleStart() {
    if (!idFirstQuestion) return;

    history.push(match.url + `/question/${idFirstQuestion}`);
  }

  function handleCreate() {
    history.push(match.url + `/question/create`);
  }

  return (
    <Fragment>
      <header className="header">
        <h1 className="header__title">{name}</h1>
        <h3 className="header__subtitle">{description}</h3>
      </header>

      <section className="container">
        <button
          className="button"
          disabled={!idFirstQuestion}
          onClick={handleStart}
        >
          Start Questionaire
        </button>
        <button
          className="button"
          style={{ marginLeft: 'var(--s-size)' }}
          onClick={handleCreate}
        >
          Create Question
        </button>
      </section>
    </Fragment>
  );
};

window.QuestionaireDetails = QuestionaireDetails;

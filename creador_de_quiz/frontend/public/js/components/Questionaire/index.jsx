const { React } = window;
const { useState, useEffect } = window.React;
const { Switch, Route, useHistory } = window.ReactRouterDOM;
const {
  QuestionaireContext,
  QuestionForm,
  Question,
  QuestionaireDetails,
  Feedback,
} = window;

const Questionaire = ({ match }) => {
  const [questionare, setQuestionare] = useState(null);
  const [answers, setAnswers] = useState([]);
  const [fetchedQuestionare, setFetchedQuestionare] = useState(false);
  const history = useHistory();

  function fetchQuestions() {
    const { GET_QUESTIONNAIRE_QUERY } = window.queries;   

    const url_params = `${GET_QUESTIONNAIRE_QUERY.query} + &variables={"id":${match.params.id}}`;

    fetch('http://ec2-34-247-162-212.eu-west-1.compute.amazonaws.com:4000/graphql?query=' + url_params)
      .then(res => res.json())
      .then(json_res => json_res.data)
      .then(({ getQuestionnaire: QUESTIONNAIRE }) => {              
        setQuestionare(QUESTIONNAIRE);
        setFetchedQuestionare(true);
      })
  }

  function onAnswer(question, answer) {
    setAnswers([...answers, answer]);

    const questionIndex = questions.indexOf(question);
    const nextQuestion = questions[questionIndex + 1];

    if (nextQuestion) {
      history.push(match.url + `/question/${nextQuestion.id}`);
    } else {
      history.push(match.url + `/feedback`);
    }
  }

  useEffect(() => {
    fetchQuestions();
  }, []);

  if (!fetchedQuestionare) {
    return (
      <header className="header">
        <h3 className="header__title">Fetching Questionare...</h3>
      </header>
    );
  }

  const { id, name, description, questions } = questionare;

  return (
    <QuestionaireContext.Provider
      value={{ id, name, description, questions, answers, onAnswer }}
    >
      <Switch>
        <Route
          path={match.path + '/question/create'}
          component={QuestionForm}
        />
        <Route path={match.path + '/question/:id'} component={Question} />
        <Route path={match.path + '/feedback'} component={Feedback} />
        <Route exact path={match.path + '/'} component={QuestionaireDetails} />
      </Switch>
    </QuestionaireContext.Provider>
  );
};

window.Questionaire = Questionaire;

const { React } = window;

const QuestionaireContext = React.createContext({
  id: -1,
  name: '',
  description: '',
  questions: [],
  answers: [],
  onAnswer: () => {
    return;
  },
});

window.QuestionaireContext = QuestionaireContext;

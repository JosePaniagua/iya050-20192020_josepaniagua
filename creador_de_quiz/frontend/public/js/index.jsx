const { React, ReactDOM, App } = window;

const element = document.getElementById('app');

ReactDOM.render(<App />, element);

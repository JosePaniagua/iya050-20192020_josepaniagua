window.queries = {
    GET_QUESTIONNAIRE_QUERY: {
        query: `query QUESTIONANAIRE($id: Int!){
            getQuestionnaire(id: $id) {
                id
                name
                description
                questions{
                    id
                    text
                    answers{
                        id
                        type
                        text
                        correct
                    }
                }
            }
        }`,
        variables: { id: 1 }
    },
    POST_QUESTION_QUERY: {
        query: `mutation addQuestion($question: QuestionInput!, $idQuestionnaire:Int!) {
            addQuestion(question: $question, questionnaireId:$idQuestionnaire) {
              text
              answers {
                text
                correct
              }
            }
          }`,
        variables: { idQuestionnaire: 1, question: {} }
    }
}
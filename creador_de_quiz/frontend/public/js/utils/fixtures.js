window.fixtures = {
  QUESTIONARE: {
    type: 'Questionare',
    id: 1,
    name: 'Test Questionare',
    description: 'Questionare for testing app functionality',
    questions: [
      {
        type: 'Question',
        id: 1,
        text: 'How are you?',
        answers: [
          {
            type: 'Answer',
            id: 1,
            idQuestion: 1,
            text: 'Good',
            correct: true,
          },
          {
            type: 'Answer',
            id: 2,
            idQuestion: 1,
            text: 'Bad',
            correct: true,
          },
        ],
      },
      {
        type: 'Question',
        id: 2,
        text: 'What is your gender?',
        answers: [
          {
            type: 'Answer',
            id: 3,
            idQuestion: 2,
            text: 'Male',
            correct: true,
          },
          {
            type: 'Answer',
            id: 4,
            idQuestion: 2,
            text: 'Female',
            correct: true,
          },
          {
            type: 'Answer',
            id: 5,
            idQuestion: 2,
            text: 'Other',
            correct: false,
          },
        ],
      },
      {
        type: 'Question',
        id: 3,
        text: 'How much is 9 + 10?',
        answers: [
          {
            type: 'Answer',
            id: 6,
            text: '4',
            correct: false,
          },
          {
            type: 'Answer',
            id: 7,
            text: '21',
            correct: false,
          },
          {
            type: 'Answer',
            id: 8,
            text: '19',
            correct: true,
          },
          {
            type: 'Answer',
            id: 9,
            text: '910',
            correct: false,
          },
        ],
      },
    ],
  },
};

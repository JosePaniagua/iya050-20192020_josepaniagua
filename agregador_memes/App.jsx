const { React, ReactDOM } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

const App = (props) => {
  return (
    <Router>
      <div id="navbar">
        <Link className="link-container" to="/memes">
          Memes list
        </Link>
        <Link className="link-container" to="/memes-form">
          Add a meme
        </Link>
      </div>
      <Switch>
        <Route path="/memes">
          <MemesList />
        </Route>
        <Route path="/meme/:id">
          <MemeView />
        </Route>
        <Route path="/memes-form">
          <MemeForm />
        </Route>
      </Switch>
    </Router>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));

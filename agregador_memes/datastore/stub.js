const memes_datastore = [
  {
    id: 1,
    source:
      "https://i.pinimg.com/originals/e7/fe/62/e7fe6297f383b5c7c1aa9b585f2cca1a.jpg",
    likes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
  {
    id: 2,
    source:
      "https://em.wattpad.com/208120825cc2288ba0c43aace25f16f4936195bd/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f6e58324d3542375a5a546d3247773d3d2d3337333835373530362e313461343434306133616438336436643931343539313538343833302e6a7067?s=fit&w=720&h=720",
    likes: [
      {
        name: "Reyhan",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Ismael",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Sean",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Csóka",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Gera",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
  {
    id: 3,
    source:
      "https://i.pinimg.com/736x/e0/b6/a6/e0b6a67b01c2c6444ae829e9aa738b78.jpg",
    likes: [
      {
        name: "Reyhan",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Ismael",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Sean",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Csóka",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Gera",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
  {
    id: 4,
    source:
      "https://pics.me.me/como-crees-que-te-ves-cuando-hablas-de-programacion-como-57275357.png",
    likes: [
      {
        name: "Reyhan",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Ismael",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Sean",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Csóka",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Gera",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
  {
    id: 5,
    source:
      "https://i.pinimg.com/736x/f3/de/02/f3de02a6402b90a111857c83b570322d.jpg",
    likes: [
      {
        name: "Reyhan",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Ismael",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Sean",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Csóka",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Gera",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
  {
    id: 6,
    source:
      "https://brandominus.com/wp-content/uploads/2015/07/10346314_477967195687241_6738430905776096079_n.jpg",
    likes: [
      {
        name: "Reyhan",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Ismael",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Sean",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Csóka",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
      {
        name: "Gera",
        image_url: "https://eu.ui-avatars.com/api/?name=John+Doe",
      },
    ],
    dislikes: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    shared: [
      {
        name: "Reyhan",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
      },
      {
        name: "Ismael",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
      },
      {
        name: "Sean",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
      },
      {
        name: "Csóka",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
      },
      {
        name: "Gera",
        avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
      },
    ],
    comments: [
      {
        user: {
          name: "Reyhan",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Reyhan+Kandemir",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Ismael",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Ismael+Becerra",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Sean",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Sean+Mills",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Csóka",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Csóka+Fülöp",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
      {
        user: {
          name: "Gera",
          avatar_url: "https://eu.ui-avatars.com/api/?name=Gera+Sámson",
        },
        comment:
          "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
      },
    ],
    description:
      "Vulputate volutpat ligula hendrerit aliquam nisl libero magna blandit",
  },
];

const current_user = {
  name: "José",
  avatar_url: "https://eu.ui-avatars.com/api/?name=José+Paniagua",
};

const user_comment =
  "Non augue sodales sociosqu aenean sagittis arcu potenti habitant sodales";

const { React } = window;
const { useHistory } = window.ReactRouterDOM;
const useState = React.useState;

const MemeForm = (props) => {
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const history = useHistory();

  const img =
    image != "" ? <img className="meme-resource" src={image} /> : null;

  const handleImageUpload = (event) => {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e) => {
        setImage(e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const postMeme = () => {
    memes_datastore.push({
      id: memes_datastore.length + 1,
      description,
      likes: [],
      dislikes: [],
      shared: [],
      comments: [],
      source: image,
    });
    history.push("/memes");
  };

  return (
    <div className="meme-form-container">
      <div className="meme-form-card">
        <div className="meme-form-description-input-container">
          <input
            type="text"
            placeholder="Meme description"
            value={description}
            onChange={() => setDescription(event.target.value)}
          />
        </div>
        <div className="meme-form-image-input-container">
          <input type="file" onChange={handleImageUpload} />
        </div>
        <div className="meme-form-image-container">{img}</div>
        <div className="meme-form-send-button-container">
          <button onClick={postMeme}>Send</button>
        </div>
      </div>
    </div>
  );
};

export default MemeForm;

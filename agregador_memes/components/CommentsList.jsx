const { React, Comment } = window;

const CommentsList = (props) => {
  const comments_list = props.comments.map((comment) => {
    return (
      <Comment
        key={comment.user.name}
        user={comment.user}
        content={comment.comment}
      />
    );
  });

  return <div className="comment-list-container">{comments_list}</div>;
};

export default CommentsList;

const { React } = window

const Comment = (props) => {
  return (
    <div className="comment">
      <div className="comment-container">
        <div className="comment-media-container">
          <img
            className="comment-user-profile-pic"
            src={props.user.avatar_url}
          />
        </div>
        <div className="comment-data-container">
          <div className="comment-user-name">
            <h4>{props.user.name}</h4>
          </div>
          <div className="comment-content">
            <p>{props.content}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Comment

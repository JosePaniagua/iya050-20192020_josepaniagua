const { React } = window;
const useState = React.useState;

const CommentForm = (props) => {
  const [text, setText] = useState('');
  const handleInputChange = (event) => {
    setText(event.target.value);
  };

  const sendComment = () => {  
    // eslint-disable-next-line
    //current_user is in stub, it should be retrived in the future from backend in login
    props.sendComment(
      props.comments.concat({
        user: current_user,
        comment: text,
      })
    );    
    setText('');
  };

  return (
    <div className="comment-form">
      <CommentsList comments={props.comments} />
      <div className="input-container">
        <div className="comment-content-container">
          <input type="text" value={text} onChange={handleInputChange} />
        </div>
        <div className="comment-button-container">
          <button className="comment-button" onClick={sendComment}>
            Commentar▶️
          </button>
        </div>
      </div>
    </div>
  )
};

export default CommentForm;

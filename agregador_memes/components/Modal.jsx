const { React } = window;

const Modal = ({ handleClose, show, children }) => {
  const className = show ? "modal display-block" : "modal display-none";
  return (
    <div className={className}>
      <section className="modal-main">
        <button className="button-close-modal" onClick={handleClose}>
          X
        </button>
        <div>{children}</div>
      </section>
    </div>
  );
};

export default Modal;

const { React, Meme } = window;
const { useParams } = window.ReactRouterDOM;

const MemeView = (props) => {
  const { id } = useParams();
  const meme = memes_datastore[id - 1];

  return (
    <div className="memes-list-container">
      <Meme
        key={meme.id}
        id={meme.id}
        source={meme.source}
        likes={meme.likes}
        dislikes={meme.dislikes}
        shared={meme.shared}
        comments={meme.comments}
        description={meme.description}
      />
    </div>
  );
};

export default MemeView;

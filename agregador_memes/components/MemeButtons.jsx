const { React, Modal, CommentForm } = window;
const useState = React.useState;

const MemeButtons = (props) => {
  const [isCommenting, setIsCommenting] = useState(false);

  const handleReactionClick = (state, fn, prop) => {
    const new_state = { pressed: !state.pressed };
    //!Important, current_user is defined in stub and using shift unshit to make
    //easier the "unpress" functionality in future implementations
    state.pressed ? state[prop].shift() : state[prop].unshift(current_user);
    new_state[prop] = state[prop];
    fn(new_state);
  };

  const handleModalDisplay = () => {
    setIsCommenting(!isCommenting);
  };

  return (
    <div className="container meme-buttons-container">
      <div
        className="container meme-button-container meme-button-like"
        onClick={() =>
          handleReactionClick(
            props.statistics.likes,
            props.actions.setLike,
            "likes"
          )
        }
      >
        <div className="emoji like-emoji">👍🏽 me gusta</div>
        <div className="container statistics" id="liked-number">
          {props.statistics.likes.likes.length}
        </div>
      </div>
      <div className="container meme-button-container meme-button-comment">
        <div className="emoji comment-emoji" onClick={handleModalDisplay}>
          📤 comment
        </div>
        <div className="container statistics" id="comments-number">
          {props.statistics.comments.length}
        </div>
        <Modal handleClose={handleModalDisplay} show={isCommenting}>
          <CommentForm
            sendComment={props.actions.setComments}
            comments={props.statistics.comments}
          />
        </Modal>
      </div>
      <div
        className="container meme-button-container meme-button-share"
        onClick={() =>
          props.actions.setShared(props.statistics.shared.concat({}))
        }
      >
        <div className="emoji share-emoji">▶️ compartir</div>
        <div className="container statistics" id="shared-number">
          {props.statistics.shared.length}
        </div>
      </div>
      <div
        className="container meme-button-container meme-button-dislike"
        onClick={() =>
          handleReactionClick(
            props.statistics.dislikes,
            props.actions.setDislike,
            "dislikes"
          )
        }
      >
        <div className="emoji dislike-emoji">👎no me gusta</div>
        <div className="container statistics" id="disliked-number">
          {props.statistics.dislikes.dislikes.length}
        </div>
      </div>
    </div>
  );
};

export default MemeButtons;

const { React, Meme } = window;

const MemesList = (props) => {
  const memes = memes_datastore.map((meme) => {
    return (
      <Meme
        key={meme.id}
        id={meme.id}
        source={meme.source}
        likes={meme.likes}
        dislikes={meme.dislikes}
        shared={meme.shared}
        comments={meme.comments}
        description={meme.description}
      />
    );
  });

  return <div className="memes-list-container">{memes}</div>;
};

export default MemesList;

const { React, MemeButtons } = window;
const useState = React.useState;
const { useHistory } = window.ReactRouterDOM;

const Meme = (props) => {
  const history = useHistory();
  const [likes, setLike] = useState({ likes: props.likes, pressed: false });
  const [dislikes, setDislike] = useState({
    dislikes: props.dislikes,
    pressed: false,
  });
  const [shared, setShared] = useState(props.shared);
  const [comments, setComments] = useState(props.comments);

  const actions = { setLike, setDislike, setComments, setShared };
  const statistics = { likes, dislikes, shared, comments };

  const viewMeme = () => {
    history.push("/meme/" + props.id);
  };

  return (
    <div className="container meme-container">
      <div className="container meme-card" onClick={viewMeme}>
        <p>{props.description}</p>
        <div className="container meme-image-container">
          <img src={props.source} />
        </div>
        <MemeButtons statistics={statistics} actions={actions} />
      </div>
    </div>
  );
};

export default Meme;
